from django.contrib import admin

# Register your models here.
from gestionPedidos.models import *

class CleintesAdmin(admin.ModelAdmin):
    list_display = ("nombre", "direccion", "email", "phone")
    search_fields = ("nombre", "direccion")
    list_filter = ("nombre", "direccion", "email", "phone")

class ArticulosAdmin(admin.ModelAdmin):
    list_display = ("nombre", "seccion", "precio")
    search_fields = ("nombre", "seccion", "precio")
    list_filter = ("nombre", "seccion", "precio")

class PedidosAdmin(admin.ModelAdmin):
    list_display = ("numero", "fecha", "entregado")
    search_fields = ("fecha", "entregado")
    list_filter = ("numero", "fecha", "entregado")
    date_hierarchy = "fecha"


admin.site.register(Clientes, CleintesAdmin)
admin.site.register(Articulos, ArticulosAdmin)
admin.site.register(Pedidos, PedidosAdmin)