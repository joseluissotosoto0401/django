from django.shortcuts import render
from django.http import HttpResponse
from gestionPedidos.forms import FormularioContacto
from django.core.mail import send_mail

from django.conf import settings

from gestionPedidos.models import *
# Create your views here.

def busqueda_productos(requets):
    
    return render(requets, "BusquedaProducto.html")


def buscar(request):
    
    
    if request.GET["prd"]:
        
        #mensaje = "Articulo buscado: %r" %request.GET["prd"]
        producto = request.GET["prd"]
        
        if len(producto)>30:
            
            mensaje = "texto de busquda demasiado largo"
            
        else:
        
            articulos = Articulos.objects.filter(nombre__icontains=producto)
            
            return render(request, "resultadosBusqueda.html", {"articulos":articulos, "query":producto})
            
    else:
        
        mensaje = "No has introducido nada"
    
    return HttpResponse(mensaje)


def contacto(request):
    
    if request.method=="POST":
        
        #subject = request.POST["asunto"]
        #message = request.POST["mensaje"] + " " + request.POST["email"]
        #email_from = settings.EMAIL_HOST_USER
        #recipient_list = ["joseluissotosoto0401@gmail.com"]
        #send_mail(subject, message, email_from, recipient_list)
        #return render(request, "gracias.html") 
        
        miform = FormularioContacto(request.POST)
        
        if miform.is_valid():
            
            miform.cleaned_data
            #send_mail(inf_form['asunto'], inf_form['mensaje'], inf_form.get(['email', '']),['joseluissotosoto0401@gmail.com'])
            
            return render(request, "gracias.html")
        
    else: 
        
        miform = FormularioContacto()
        
    return render(request, "form_contacto.html", {"form":miform})    
    
    