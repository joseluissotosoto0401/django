from tabnanny import verbose
from django.db import models

# Create your models here.

class Clientes(models.Model):
    nombre = models.CharField(max_length = 30, verbose_name="Nombre del cliente")
    direccion = models.CharField(max_length = 50)
    email = models.EmailField(blank=True, null=True)
    phone = models.CharField(max_length=10)
    
    #def __str__(self):
    #    return 'Nombre: %s, direccion: %s, email: %s, telefono: %s' %(self.nombre, self.direccion, self.email, self.phone)

    
class Articulos(models.Model):
    nombre = models.CharField(max_length = 30, verbose_name="Nombre del articulo")
    seccion = models.CharField(max_length = 20)
    precio = models.IntegerField()
    
    #def __str__(self):
     #   return 'Nombre: %s, seccion: %s, precio: %s' %(self.nombre, self.seccion, self.precio)
    
class Pedidos(models.Model):
    numero = models.IntegerField(verbose_name="Numero del pedido")
    fecha = models.DateField()
    entregado = models.BooleanField(verbose_name="Estado del pedido") 
    
    #def __str__(self):
    #   return 'Numero: %s, fecha: %s, estado entregado: %s' %(self.numero, self.fecha, self.entregado)