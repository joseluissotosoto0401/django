from datetime import datetime
import imp
from django.template import Template, Context
from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render


class Persona(object):
    def __init__(self, nombre, apellido, edad):
        self.nombre = nombre
        self.apellido = apellido
        self.edad = edad


def saludo(request):

    p1 = Persona("Jose Luis", "Soto Soto", 21)
    temas2= []
    fecha_actual = datetime.now()

    #sin cargador
    doc_externo = open("plantillas\miPlantilla.html")
    plt = Template(doc_externo.read())
    doc_externo.close
    #ctx = Context({"nombre": p1.nombre, "apellido": p1.apellido, "edad": p1.edad, "Fecha_actual": fecha_actual})
    ctx = Context({"persona": p1, "Fecha_actual": fecha_actual, "temas": [
                  "Plantillas", "Modelos", "Formularios", "Vistas", "Despliegue"],
                   "temas2":temas2})
    documento = plt.render(ctx)
    return HttpResponse(documento)


def home(request):
    p1 = Persona("Jose Luis", "Soto Soto", 21)
    temas2= []
    fecha_actual = datetime.now()

    #con cargadores
    #doc_externo = loader.get_template('miPlantilla.html')

    #no utiliza Context
    ctx = {"persona": p1, "Fecha_actual": fecha_actual, "temas": [
                  "Plantillas", "Modelos", "Formularios", "Vistas", "Despliegue"],
                   "temas2":temas2}
    #documento = doc_externo.render(ctx)
    
    #Con modulo Shotcuts y render
    
    return render(request, "miPlantilla.html", ctx)


def suecia(request):
    fecha_actual = datetime.now()
    ctx = {"Fecha_actual": fecha_actual}
    
    return render(request, "Suecia.html", ctx)

def islandia(request):
    fecha_actual = datetime.now()
    ctx = {"fecha": fecha_actual}
    
    return render(request, "Islandia.html", ctx)

def dameFecha(request):

    fecha_actual = datetime.now()
    documeto1 = """<html>
                <body>
                <h1>
                Fecha y hora actual %s
                </h1>
                </body>
                </html>""" % fecha_actual

    return HttpResponse(documeto1)


def calculaEdad(request, agno, edad):

    #edadAcual = 21
    periodo = agno-2022
    edadFutura = edad + periodo
    documeto = "<html><body><h2>En el año %s tendrás %s años</h2></body></html>" % (
        agno, edadFutura)

    return HttpResponse(documeto)
